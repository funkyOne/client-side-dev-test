var restify = require('restify');
var DB = require("./db");

var server = restify.createServer();
server.use(restify.plugins.bodyParser());

DB.ensureInitialized();

server.get('/catalog', function (req, res, next) {
  var db = DB.load();
  res.send(db.catalog);
  next();
});

server.post('/order', function (req, res, next) {
  var order = req.body;
  var db = DB.load();
  var requestShortcutted = false;

  var validationErrors = validate(db, order);

  if (validationErrors.length) {
    res.send(400, validationErrors);
    next();
    return;
  }

  save(db, order);

  res.send(201, {
    id: order.id
  });

  next();
});

server.get('/reset', function (req, res, next) {
  DB.reset();
  res.send(200);
  next();
});

function validate(db, order) {  
  var errors = [];

  if(isNaN(order.total)){
    errors.push("Order should have total price")
  }
  
  order.items.forEach(function (item, i) {
    if (item.quantity < 1) {
      errors.push("item #" + i + ": quantity can't be less than 1")
    } else {
      var inventoryEntry = db.inventory.find(i => i.productId === item.productId);                                                      
      if ((inventoryEntry.quantity - item.quantity) < 0) {
        errors.push("item #" + i + ": not enough stock")
      }
    }
  })

  return errors;
}

function save(db, order) {
  order.items.forEach(function (item) {
    var inventoryEntry = db.inventory.find(i => i.productId === item.productId);
    inventoryEntry.quantity -= item.quantity;
  })

  order.id = db.orders.length;
  db.orders.push(order);
  DB.save(db);
}

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});