# Client-side developer test
The task is to write client-side order entry app. For API server implementation details see [Readme.md](./Readme.md).

## Overview
The app is to be used by employees on-site and it must be able to work offline. The app consist of two pages. Home page and New Order page. 

### Home page
Home page should contain **New Order** button and a table with exising orders having these columns:
    
```
Id, Customer Name, Total, Created Date, Status
```

When button is clicked user should be navigated to the New Order page.
### New Order page
This page should collect the following information:

* customer name
* order items

Order total should be auto-calculated from order items.

On submit the order should be saved locally and have `awaiting sync` status assigned. After, a sync attempt must be peformed.

### Offline
If app is currently offline the order sending must be postponed until back online. Whenever app is back online queueed orders must be sent to server.

`IMPORTANT:` Store stock is limited, so orders must be sent to server strictly in the order they were created.

### After order synced 
Once an order is submitted its status should be changed according to sync result (`rejected` or `placed`) and this change reflected in the home page table.

## Libs and Frameworks
Use one of these UI libs: Angular(1.x or 2+), React, Aurelia, Ember.js, Vue.js, Cycle.js.
For styling a CSS framework should be used, preferably Twitter Bootstrap.