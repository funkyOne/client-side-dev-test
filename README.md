# API Server for the client-side developer test

This repo contains API server for the task decscribed in [Task.md](./Task.md).

## how to setup
```
yarn
```
or 
```
npm install
```
## how to run
```
npm run start
```


### API

`GET /catalog` will return list of available products.

`POST /order` use this method to submit orders. Example body:

```
{
    "items": [
        {
            "productId": 1,
            "quantity": 10
        },
        ...
    ],
    "total": 45
}
```
If there are validation errors the method will return BadRequest with list of errors in response body. Example:
```
{    
    "item #0: not enough stock"
}
```
On success the method will return 201 HTTP code and order header in response body. Example:
```
{
    "id":4
}
```

`GET /reset` will reset the database to its initial state.