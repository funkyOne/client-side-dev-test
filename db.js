var fs = require('fs');

function save(db) {
    fs.writeFileSync('./data.json', JSON.stringify(db));
}

function load() {
    return JSON.parse(fs.readFileSync('./data.json'))
}

function reset() {
    var dbString = fs.readFileSync('./data-tpl.json');

    return fs.writeFileSync('./data.json', dbString);
}

function ensureInitialized() {
    if (!fs.existsSync('./data.json')) reset();
}

module.exports = {
    save: save,
    load: load,
    reset: reset,
    ensureInitialized: ensureInitialized
}